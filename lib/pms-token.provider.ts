import { TokenProvider } from "@onemineral/pms-js-sdk";

export class PmsTokenProvider implements TokenProvider {
  public get(): string {
    return process.env.PMS_API_TOKEN || "";
  }
}

const defaultPmsTokenProvider = new PmsTokenProvider();
export { defaultPmsTokenProvider };
