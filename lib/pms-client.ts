import { newPmsClient } from "@onemineral/pms-js-sdk";
import { defaultPmsTokenProvider } from "./pms-token.provider";

const pmsClient = newPmsClient({
  baseURL: process.env.PMS_BASE_URL + "/rest/",
  tokenProvider: defaultPmsTokenProvider,
});

export default pmsClient;
