import { Property } from "@onemineral/pms-js-sdk";
import type {
  GetServerSidePropsContext,
  InferGetServerSidePropsType,
} from "next";
import Image from "next/image";
import pmsClient from "../lib/pms-client";
import styles from "../styles/Home.module.css";

const Properties = ({
  data,
}: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  return (
    <div className={styles.container}>
      {data.properties.map((p: Property) => {
        <div>
          <h1>{p.name.en}</h1>

          <Image src={p.main_image.medium} alt={`${p.name.en} main image`} />
        </div>;
      })}

      <div>
        <p>Current page: {data.current_page}</p>
        <p>Per page: {data.perpage}</p>
      </div>
    </div>
  );
};

export const getServerSideProps = async (
  context: GetServerSidePropsContext
) => {
  const { query } = context;
  const { page = "1", perpage = "20" } = query;

  // Query paginated properties sorted desc by `en` locale.
  // Also include images and amenities in the response
  const { response } = await pmsClient.property.query({
    paginate: {
      page: Number(page),
      perpage: Number(perpage),
    },
    sort: [
      {
        field: "name",
        direction: "desc",
        locale: "en",
      },
    ],
    with: ["images", "amenities"],
  });

  return {
    props: {
      data: {
        properties: response.data,
        current_page: response.current_page,
        perpage: response.per_page,
      },
    },
  };
};

export default Properties;
