import { PaginatedResponse, Property } from "@onemineral/pms-js-sdk";
import type { NextApiRequest, NextApiResponse } from "next";
import pmsClient from "../../lib/pms-client";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<PaginatedResponse<Property>>
) {
  const { page, perpage } = req.query;
  // Fetch paginated properties with images
  const response = await pmsClient.property.query({
    paginate: {
      page: Number(page),
      perpage: Number(perpage),
    },
    with: ["images"],
  });

  res.status(200).json(response);
}
